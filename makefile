# Docker compose configuration file
COMPOSE_FILE=docker-compose.yml

# Runs the Docker containers
up: create_logs
	docker-compose -f $(COMPOSE_FILE) up -d

# Builds and runs the Docker containers
build: create_logs
	docker-compose -f $(COMPOSE_FILE) up -d --build

# Stops the running containers
down:
	docker-compose -f $(COMPOSE_FILE) down

# Create log files for NGINX
create_logs:
	mkdir -p docker/nginx/logs
	touch docker/nginx/logs/access.log && touch docker/nginx/logs/error.log