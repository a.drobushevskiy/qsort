# Qsort - quick sort, Tony Hoare sort. PHP implementation.

## Getting started
Rename .enx.example to .env, and specify your options if necessary.
Make a build to get the Docker containers up and running.

```
make build
```


Add `php.local` to hosts your OS.

Launch containers.

```
make up
```

Great job. In the browser you can open our project http://php.local