<?php

$numbers = [];

for ($i = 0; $i < 10000; $i++) {
    $numbers[] = rand(0, 100);
}

/**
 * @param array $numbers
 * @return array
 */
function quickSort(array $numbers): array
{
    $numbersCount = count($numbers);

    if ($numbersCount <= 1) {
        return $numbers;
    }

    $index = intval($numbersCount / 2);
    $pivot = $numbers[$index];

    $leftNumbers = array_values(array_filter($numbers, function ($x) use ($pivot) {
        return $x < $pivot;
    }));

    $middleNumbers = array_values(array_filter($numbers, function ($x) use ($pivot) {
       return $x == $pivot;
    }));

    $rightNumbers = array_values(array_filter($numbers, function ($x) use ($pivot) {
        return $x > $pivot;
    }));

    return array_merge(quickSort($leftNumbers), $middleNumbers, quickSort($rightNumbers));
}

$result = quickSort($numbers);

var_dump($result);